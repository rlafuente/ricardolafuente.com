#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Ricardo Lafuente'
SITENAME = 'Ricardo Lafuente'
SITEURL = 'https://ricardolafuente.com'

PATH = 'content'

TIMEZONE = 'Europe/Lisbon'

DEFAULT_LANG = 'en'

STATIC_PATHS = [
    'images', 'editorslab', 'pyporto',
    'extra/htaccess',
]
EXTRA_PATH_METADATA = {
    'extra/htaccess': {'path': '.htaccess'},
}
ARTICLE_EXCLUDES = ['editorslab', 'pyporto']



# Static pages setup
STATIC_SAVE_AS = '{path}'
STATIC_URL = '{path}'
# Set up clean URLs
ARTICLE_URL = '{category}/{slug}/'
ARTICLE_SAVE_AS = '{category}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

THEME = "ultramnmlist"

PIWIK_URL = "piwik.manufacturaindependente.org"
PIWIK_SITE_ID = 4

# Disable cache for metadata parsing
# https://docs.getpelican.com/en/stable/content.html#file-metadata
LOAD_CONTENT_CACHE = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

DEFAULT_PAGINATION = False

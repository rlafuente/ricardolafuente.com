Title: Bio
URL:
save_as: index.html

I'm a designer, hacker and lecturer. This is my personal space on the Internet, and I'm in the process of moving my old things here.


Things I do
-----------

* I'm teaching design for the web and interfaces at the Design Department of
  [ESAP](http://esap.pt).
* I'm one of the founders of the Porto chapter of
  [Journalism++](http://jplusplus.pt), the network of data-driven storytelling
  agencies. There, I carry out the role of lead editor and data architect.
* I run the [Manufactura Independente](http://manufacturaindependente.org)
  design studio along with [Ana Isabel Carvalho](http://anacarvalho.org).
* I help manage and facilitate the activities of Portugal's open data
  collective, [Transparência Hackday
  Portugal](http://transparenciahackday.org).
* I get a kick out of writing code to make computers do what I want them to do,
  and most often [Python](http://python.org) is my language of choice. In the
  last decade I wrote a [tool for drawing with code](http://shoebot.net), a
  [small toolchain](http://github.com/ManufacturaInd/tinytypetools) for working
  with fonts, a [data portal](http://centraldedados.pt) an old-school [ASCII
  art image renderer](http://github.com/ManufacturaInd/badger) and a good
  amount of public information [scrapers](https://github.com/centraldedados).
  Most of the code I wrote can be found at my [GitLab
  page](http://gitlab.com/rlafuente).
* I'm an advocate for copyleft culture, libre design and open data, and spent
  the better part of the last decade incorporating those principles into my
  practice. I use free software for all my design and code work.


Things I did
------------

* I was one of the co-editors and designers of [Libre Graphics
  magazine](http://libregraphicsmag.com), a publication dedicated to the
  intersections between free culture and design published between 2010 and 2016.
* I was a lecturer at the Design department of the [Faculty of Fine Arts of the
  University of Porto](http://fba.up.pt) between 2010 and 2014, having planned
  and taught courses on hardware hacking for artists, introduction to digital
  tools, intermediate web design, intellectual property and Python coding for
  designers.
* I helped run [Hacklaviva](http://hacklaviva.net), Porto's first hackerspace.
* I developed Lightwriter, a media art piece which was showcased in several
  places in Europe. I'm redocumenting that work at the moment.
* I've spoken regularly at events related to open data, communication design,
  typography and free culture.

Elsewhere
---------

* I publish my code on [GitLab](https://gitlab.com/rlafuente) and tweet as
  [@rlaf](http://twitter.com/rlaf). These days I prefer Mastodon, and you'll
  find my toots at [post.lurk.org/rlafuente](https://post.lurk.org/@rlafuente)
* IRC was very close to me as I grew up, and the late evenings at
  [PTnet](http://ptnet.org) are a fond memory. I now lurk as luluganeta on
  [Freenode](http://freenode.net).
* My e-mail address is ricardo at manufacturaindependente dot org. I have
  others, but they all end up in the same place.

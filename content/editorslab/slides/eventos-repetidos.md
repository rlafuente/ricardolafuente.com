### Coisas que acontecem mais de uma vez

Eleições, tempestades, campeonatos do mundo de futebol, inundações, incêndios, jogos olímpicos, investigações policiais, remodelações ministeriais, jogos da Primeira Liga, comissões parlamentares, campeonatos de ténis, julgamentos, derrames de petróleo, lançamentos de produtos Apple, prémios Nobel, assassinatos, debates eleitorais, escândalos ambientais, novos modelos de automóveis, bombardeamentos dos EUA

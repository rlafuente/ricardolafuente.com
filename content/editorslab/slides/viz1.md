### Visualização de dados

* [Datawrapper](http://datawrapper.de/)
* [Chartbuilder](http://quartz.github.io/Chartbuilder/)
* [RAW](http://raw.densitydesign.org/)
* [Highcharts](http://highcharts.com/)
* [TableauPublic](http://public.tableausoftware.com/)
* [Infogram](http://infogr.am)
* [Raphael](http://raphaeljs.com/)
* [D3.js](http://d3js.org)

### Pelican

- ready-to-use Makefile
- multi-language support
- Jinja templates
- Markdown files with meta fields

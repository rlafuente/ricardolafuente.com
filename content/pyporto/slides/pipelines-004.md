### csvkit

```bash
# we only need some of the columns
# also convert to utf-8, remove "xxxx" entries and fix typos
csvcut companies-full.csv --encoding iso-8859-1 --columns 1,2,3,4,5,6,7,8,11,12,23,24,25,26,27 --delete-empty-rows
```

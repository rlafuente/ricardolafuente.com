### make install

Use virtualenvs transparently!

```makefile
ACTIVATE=. `pwd`/.env/bin/activate

install:
	virtualenv .env --python=/usr/bin/python3 --no-site-packages --distribute
	$(ACTIVATE); pip install -r requirements.txt
```

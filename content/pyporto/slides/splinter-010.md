#### Tirar screenshots

```python
browser.visit(url)
    if browser.status_code.is_success():
        filename = url.replace('http://localhost:8080/', '').replace('.html', '') + '.png'
        browser.driver.save_screenshot(filename)
```



```python
    env = jinja2.Environment(loader=jinja2.FileSystemLoader([template_dir]))

    template = env.get_template("list.html")
    target = "index.html"
    context = {"datapackages": packages,
               "welcome_text": markdown.markdown(
                   codecs.open("content/welcome_text.md", 'r', 'utf-8').read(),
                   output_format="html5", encoding="UTF-8"),
               }
    contents = template.render(**context)
    f = codecs.open(os.path.join(output_dir, target), 'w', 'utf-8')
    f.write(contents)
    f.close()
    print("Created index.html.")
```

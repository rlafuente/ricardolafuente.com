### make build

```makefile
build:
	./massage.sh && \
	. `pwd`/.env/bin/activate && python complement.py \
	./massage-transfers.sh && \
	. `pwd`/.env/bin/activate && python complement-transfers.py && \
	./extract.sh && \
	. `pwd`/.env/bin/activate && python compute-facts.py

```

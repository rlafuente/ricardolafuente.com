### make css

```makefile
BUILD_DIR = dist
SASS_FILES_DIR = src/css
TARGET_CSS_DIR = $(BUILD_DIR)/css
CSS_FILES=$(patsubst $(SASS_FILES_DIR)/%.scss,$(TARGET_CSS_DIR)/%.css,$(SASS_FILES))
SASS=./lib/sass
SASS_FLAGS = --style compressed --quiet

css: $(CSS_FILES)

$(TARGET_CSS_DIR)/%.css: $(SASS_FILES_DIR)/%.scss
	$(SASS) $(SASS_FLAGS) $< > $@
```

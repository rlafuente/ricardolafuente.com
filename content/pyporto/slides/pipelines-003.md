### bash and sed
```bash
cat companies-full.csv | \
  sed 's/generaldynamics.com,1,1/generaldynamics.com,1,0/g' | \
  sed 's/wynyardgroup.com,,/wynyardgroup.com,,1/g' | \
  sed 's/reach-u.com\/,,/reach-u.com,,1/g' | \
  sed 's/providenceitf.com,,/providenceitf.com,,1/g' | \
  sed 's/EC2A 4RR, United Kingdom",,Phone Monitoring,,,,,,/EC2A 4RR, United Kingdom",,Phone Monitoring,,,1,,,/g' | \
  sed 's/tinex\.no,0,1/tinex.no,0,0/g' | \
  > companies-filtered.csv
```

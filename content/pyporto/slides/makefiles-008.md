### make deploy

```makefile
deploy: build
	rsync -arvu --delete dist/* server:/home/rlafuente/public_html/
dry-deploy: build
	rsync -arvun --delete dist/* server:/home/rlafuente/public_html/
```

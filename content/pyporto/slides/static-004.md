```markdown
Title: The Confit of Interest
Summary: Mix together private interest and public money for the yummiest of all recipes!
Tags: highlight, main course
Slug: conflict-of-interest
Lang: en
Risk: Low
Duration: Under 5 years
Ingredients: 
	A public works company
    A high-ranking position in a civil administration
Serves: up to €10 million

**1. Access a position of power within a public administration**. You do not have to be in control of finances, but you need to have enough power to exchange favors. Being in the bureau of your party is a good choice, because you can decide who gets the top spot on the list on election day. 

**2. (Optional) Change the nominal owner of your company.** You can give your company to your wife or to a shell company in a tax-heaven (in a tax heaven that does not disclose the name of the owner). You can also steal the passport of a random person (or find one on Google Images) and create a company in Delaware under this person's name.
```
